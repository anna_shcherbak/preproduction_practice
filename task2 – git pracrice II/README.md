>Additional task (*)
>1)	Create 4 commits into master branch with minor changes in file that is already exist.
>2)	Execute command “git reset --hard HEAD~4”
>3)	Are you able to restore changes of third commit? Describe your steps how to do that.

I created 4 new commits in main branch and after having done this, I executed *"git reset --hard HEAD~4"* command.

![Screenshot1]( ./images/Screenshot1.png)

First of all, in order to restore changes of third commit, I executed  *"git reflog"* command to view the hash of the commit that needs to be restored.
Than, I executed *"git checkout  HEAD@{4}"* to move to the third commit by detaching HEAD. Now, I can create new branch here to create a reference to this commit with *"git checkout -b restored"*.
